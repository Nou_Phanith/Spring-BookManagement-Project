CREATE TABLE tb_book(
  id SERIAL PRIMARY KEY,
  title varchar,
  author varchar,
  publisher varchar,
  thumbnail varchar

);