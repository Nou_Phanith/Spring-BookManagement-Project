package com.example.demo.Services;

import com.example.demo.Models.BookModels;
import java.util.List;
import com.example.demo.Models.BookModels;
import java.util.List;

public interface BookInterface {

        List<BookModels> getAll();

        BookModels view(Integer id);

        boolean delete(Integer delete_id);

        boolean create(BookModels bookModels);

        boolean update(BookModels bookModels);
    }




