package com.example.demo.Services;

import com.example.demo.Models.BookModels;
import com.example.demo.Respositories.BookRepositories;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BookServices implements BookInterface {

    @Autowired
    BookRepositories bookRepositories;

    public BookServices(BookRepositories bookRepositories) {
        this.bookRepositories = bookRepositories;
    }
    public BookRepositories getRepositories() {
        return bookRepositories;
    }

    public void setRepositories(BookRepositories repositories) {
        this.bookRepositories = repositories;
    }

    @Override
    public List<BookModels> getAll() {
        return this.bookRepositories.getListBookModels();
    }

    @Override
    public BookModels view(Integer id) {
        return this.bookRepositories.view(id);
    }

    @Override
    public boolean delete(Integer delete_id) {
        return this.bookRepositories.delete(delete_id);
    }

    @Override
    public boolean create(BookModels bookModels) {
        return this.bookRepositories.create(bookModels);
    }
    @Override
    public boolean update(BookModels bookModels) {
        return this.bookRepositories.update(bookModels);
    }

    public boolean count(Integer id) {
        return this.bookRepositories.count(id);
    }

}
