package com.example.demo.Models;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class BookModels {

    //No validation cuz it is auto increasement
    private int id;
//    @Size(min=2 ,max=50)
    @NotEmpty (message = "Contain Empty not Allowed")
    private String title;
    @Size(min=2 ,max=50)
    private String author;
    @Size(min=2 ,max=50)
    private String publisher;
    private String thumbnail;

    private String categories;
    public BookModels() {
    }

    public BookModels(int id, String title, String author,String thumbnail,String categories) {
        this.id = id;
        this.title = title;
        this.author = author;
        this.publisher= publisher;
        this.thumbnail = thumbnail;
        this.categories=categories;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getCategories() {
        return categories;
    }

    public void setCategories(String categories) {
        this.categories = categories;
    }

//    public String getPublisher() {
//        return publisher;
//    }
//
//    public void setPublisher(String publisher) {
//        this.publisher = publisher;
//    }


    @Override
    public String toString() {
        return "BookModels{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", author='" + author + '\'' +
                ", publisher='" + publisher + '\'' +
                ", thumbnail='" + thumbnail + '\'' +
                ", categories='" + categories + '\'' +
                '}';
    }
}
