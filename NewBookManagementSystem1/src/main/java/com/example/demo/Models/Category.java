package com.example.demo.Models;

import javax.validation.constraints.NotNull;
import java.util.List;

public class Category {


    private Integer id;
    private String name;

    private List<BookModels> listBookModels;

    public Category(Integer id, String name, List<BookModels> listBookModels) {
        this.id = id;
        this.name = name;
        this.listBookModels = listBookModels;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<BookModels> getListBookModels() {
        return listBookModels;
    }

    public void setListBookModels(List<BookModels> listBookModels) {
        this.listBookModels = listBookModels;
    }

    @Override
    public String toString() {
        return "Category{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", listBookModels=" + listBookModels +
                '}';
    }
}
