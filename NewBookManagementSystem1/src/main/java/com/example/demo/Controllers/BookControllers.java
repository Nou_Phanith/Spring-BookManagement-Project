package com.example.demo.Controllers;

import com.example.demo.Models.BookModels;
import com.example.demo.Services.BookServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.awt.print.Book;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.UUID;

@Controller
public class BookControllers {

    BookServices bookServices;

    @Autowired
    public BookControllers(BookServices bookServices) {
        this.bookServices = bookServices;
    }

    //    Map To url to display the data vai @GetMapping Annotation
    @GetMapping("/index")
    public String getIndex(Model model) {

        List<BookModels> bookModels = this.bookServices.getAll();
        model.addAttribute("books", bookModels);
        return "/Index";
    }

    //    Mapping to Delete Method
    @GetMapping("/delete/{delete_id}")
    public String delete(@PathVariable("delete_id") Integer id, Model model) {
        this.bookServices.delete(id);
        return "redirect:/index";
    }

    //    Mapping to view method
    @GetMapping("/view/{id}")
    public String view(@PathVariable("id") Integer id, Model model) {
        BookModels bookModels = this.bookServices.view(id);
        model.addAttribute("book", bookModels);
        return "/view";
    }

    @PostMapping("/index")
    public String GoToIndex() {
        return "/index";
    }

    //    Mapping to Update Method (1.View In Form)
    @GetMapping("/update/{book_id}")
    public String update(@PathVariable Integer book_id, ModelMap modelMapl) {
        BookModels bookModels = this.bookServices.view(book_id);
        modelMapl.addAttribute("book", bookModels);
        return "/update";
    }

    //  Mapping to Update Method (2. Submit After Editing)
    @PostMapping("/update/submit")
    public String submit(@ModelAttribute("book") @Valid BookModels bookModels,BindingResult bindingResult, MultipartFile file) {

        if(bindingResult.hasErrors())
        {
            System.out.println("Update found the error!");
            return "/update";
        }
        String fileName = "";
        File path = new File("/btb");
        if (!path.exists()) {
            path.mkdirs();
        }

        if (!file.isEmpty()) {
            fileName = file.getOriginalFilename();
//       Create String extension to catch extension File
            String extension = fileName.substring(fileName.lastIndexOf('.')+1);
//        System.out.println(fileName+' '+extension);
            fileName = UUID.randomUUID() + "." + extension;
            try {
                Files.copy(file.getInputStream(), Paths.get(("c:/btb"), fileName));
            } catch (IOException e) {
                e.printStackTrace();
            }
            bookModels.setThumbnail("/imgae-btb/" + fileName);
        }

        this.bookServices.update(bookModels);
        System.out.println(bookModels);
        return "redirect:/index";
    }
    //    Closing Update Function

    // Creat a new data method
    @GetMapping("/create")
    public String create(Model model) {
        model.addAttribute("book", new BookModels());
        return "/createBook";
    }

    @PostMapping("/create/submit")
    public String create(@Valid BookModels bookModels, BindingResult bindingResult, MultipartFile file, Model model) {

        // Checking the error by using BindingResult
        if (bindingResult.hasErrors()) {
            model.addAttribute("book", new BookModels());
            return "/createBook";
        }
        if (file == null) {
            return null;
        }
        System.out.println("testasdas");

       File path = new File("/btb");
        if (!path.exists()) {
            path.mkdirs();
        }

            String fileName = file.getOriginalFilename();
            String extension = fileName.substring(fileName.lastIndexOf('.')+1);
            System.out.println(fileName + ' ' + extension);
            //Craete UUID to randome the file name without duplicated
            fileName = UUID.randomUUID() + "." + extension;
            try {
                Files.copy(file.getInputStream(), Paths.get(("c:/btb"), fileName));
            } catch (IOException e) {
                e.printStackTrace();
            }
            //Set thumbnail in case there is no error
            bookModels.setThumbnail("/imgae-btb/" + fileName);

        this.bookServices.create(bookModels);
        return "redirect:/index";
    }
    //Closing Create Method ---

    //Count Recore Function


    //  Count record close

}
//Closing BookControlls Main---
