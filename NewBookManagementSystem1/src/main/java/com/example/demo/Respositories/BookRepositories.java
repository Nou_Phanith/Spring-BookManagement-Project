package com.example.demo.Respositories;


import com.example.demo.Models.BookModels;
import com.github.javafaker.Faker;
import org.apache.ibatis.annotations.*;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public interface BookRepositories {

    @Select("SELECT * FROM tb_book")
    public List<BookModels> getListBookModels();

    @Select("SELECT *FROM tb_book WHERE id=#{id}")
    BookModels view(@Param("id") Integer id);

    @Update("UPDATE tb_book SET title=#{title},author=#{author},publisher=#{publisher},thumbnail=#{thumbnail} WHERE id=#{id}")
    boolean update(BookModels bookModels);

    @Delete("DELETE FROM tb_book WHERE id=#{id}")
     boolean delete(Integer id);

    @Insert("INSERT INTO tb_book(title,author,publisher,thumbnail) " +
            "VALUES(#{title},#{author},#{publisher},#{thumbnail})")
     boolean create(BookModels bookModels);

    @Select("SELECT COUNT (id) FROM tb_book ")
    boolean count(Integer id);

}
//Closing Main Class
















//    Creating Java faker to generate data
//    Faker faker = new Faker();
//    //    Creating list to store the data
//    List<BookModels> listBookModels = new ArrayList<>();
//    {
//        for(int i=1; i<11; i++)
//        {
//            BookModels bookModels = new BookModels();
//
//            bookModels.setId(i);
//            bookModels.setTitle(faker.book().title());
//            bookModels.setAuthor(faker.book().author());
//            bookModels.setAuthor(faker.book().publisher());
//
//            listBookModels.add(bookModels);
//        }
//    }

//    Create list reference to Model
//    public List<BookModels> getListBookModels() {
//        return this.listBookModels;
//    }
//    //    View All Data Function
//    public BookModels view(Integer id)
//    {
//        for(int i=0; i<listBookModels.size();i++)
//        {
//            if(listBookModels.get(i).getId()==id)
//            {
//                System.out.println(listBookModels.get(i));
//                return listBookModels.get(i);
//            }
//        }
//        return null;
//    }
//    //    View Data Ended -----
//    //    Update Function
//    public boolean update(BookModels bookModels)
//    {
//        for(int i=0; i<listBookModels.size(); i++)
//        {
//    //  Update the data by compare to the ID
//            if(listBookModels.get(i).getId()==bookModels.getId())
//            {
//                listBookModels.set(i,bookModels);
//                return true;
//            }
//        }
//        return false;
//    }
//
//    //    Update Fucntion Ended ----
//
//    //Delete Fucntion
//    public boolean delete(Integer delete_id)
//    {
//        for(int i=0; i<listBookModels.size(); i++)
//        {
//    //  Delete the Data by comparing the ID
//            if(listBookModels.get(i).getId()==delete_id)
//            {
//                listBookModels.remove(i);
//                return true;
//            }
//        }
//        return false;
//    }
//    //    Create Function Ended
//
//    public boolean create(BookModels bookModels)
//    {
//    // Creating a new Book by Add to ArrayList
//        listBookModels.add(bookModels);
//        return listBookModels.add(bookModels);
//    }
//Create Closed----